<!doctype html>
<html lang="en">
<head>
    <title>Fale Conosco</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-sm-12 col-12 m-auto">
            <form action="{{route('send-email')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card shadow">

                    @if(Session::has("success"))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close">&times;</button>{{Session::get('success')}}</div>
                    @elseif(Session::has("failed"))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close">&times;</button>{{Session::get('failed')}}</div>
                    @endif

                    <div class="card-header">
                        <h4 class="card-title">Fale Conosco</h4>
                    </div>

                    <div class="card-body">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" name="nome" id="nome" class="form-control"
                                   placeholder="Nome">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" class="form-control"
                                   placeholder="E-mail">
                        </div>

                        <div class="form-group">
                            <label for="emailBody">Menssagem</label>
                            <textarea name="emailBody" id="emailBody" class="form-control"
                                      placeholder="Mail Body"></textarea>
                        </div>

                        {{--                        <div class="form-group">
                                                    <label for="emailAttachments">Attachment(s) </label>
                                                    <input type="file" name="emailAttachments[]" multiple="multiple" id="emailAttachments" class="form-control">
                                                </div>--}}
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
