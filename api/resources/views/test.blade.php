<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Test Mid Level</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
</head>
    <style>
        input[type=number] {
            width: 33%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=text] {
            width: 33%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        select {
            width: 175px;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        div {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }

        table {
            border-collapse: collapse;
        }

        .center {
            text-align: center;
            color: red;
            font-weight: bold;
        }

        textarea {
            width: 100%;
            height: 150px;
            padding: 12px 20px;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
            font-size: 16px;
            resize: none;
        }
    </style>

    <body class="antialiased">
        <div style="">
            <h2>1 - Calculator</h2>
            <form method="post" action="{{ route('calculator.calculate') }}">
                @csrf
                <input type="number" placeholder="Enter with number 1" name="num1" required>
                <select name="operator">
                    <option value="">- Select operator -</option>
                    <option value="+">Plus</option>
                    <option value="-">Minus</option>
                    <option value="*">Times</option>
                    <option value="/">Divided by</option>
                </select>
                <input type="number" placeholder="Enter with number 2" name="num2" required>

                @isset($result)
                    <p class="center">Result: {{ $result }}</p>
                @endisset
                <input type="submit" value="Calculate">
            </form>
        </div>
        <hr>
        <div>
            <h2>2 - Prime Numbers</h2>
            <form method="post" action="{{ route('prime.prime-numbers') }}">
                @csrf

                <ul class="center">
                    @isset($primeNumbers)
                        @foreach ($primeNumbers as $prime)
                            <li>{{ $prime }}</li>
                        @endforeach
                    @endisset
                </ul>

                <input type="submit" value="Generate Prime Numbers">
            </form>
        </div>
        <hr>
        <div>
            <h2>3 - Factorial</h2>
            <form method="post" action="{{ route('factorial.calculate') }}">
                @csrf
                <input type="number" placeholder="Enter with number" name="number" required>
                @isset($factorial)
                    <p class="center">Factorial of {{ $number }} is: {{ $factorial }}</p>
                @endisset
                <input type="submit" value="Calculate Factorial">
            </form>
        </div>
        <hr>
        <div>
            <h2>4 - Palindrome</h2>
            <form method="post" action="{{ route('palindrome.check') }}">
                @csrf
                <input type="text" placeholder="Enter with world" name="word" required>

                @isset($isPalindrome)
                    @if ($isPalindrome)
                        <p class="center"><span style="font: 12px bold; color: black; ">{{ strtoupper($word) }}</span> is a
                            palindrome.</p>
                    @else
                        <p class="center"><span style="font: 12px bold; color: black; ">{{ strtoupper($word) }}</span> is not a
                            palindrome.</p>
                    @endif
                @endisset
                <input type="submit" value="Check Palindrome">
            </form>
        </div>
        <hr>
        <div>
            <h2>5 - Multiplication Table</h2>
            <form method="post" action="{{ route('table.generate') }}">
                @csrf
                <input type="number" placeholder="Enter with number" name="number" required>

                @isset($tableData)
                    <h3>Multiplication Table for {{ $number }}</h3>
                    <table>
                        <tr>
                            <th>Multiplier</th>
                            <th>Result</th>
                        </tr>
                        @foreach ($tableData as $row)
                            <tr>
                                <td>{{ $row['multiplier'] }}</td>
                                <td>{{ $row['result'] }}</td>
                            </tr>
                        @endforeach
                    </table>
                @endisset

                <input type="submit" value="Generate Table">
            </form>
        </div>
        <hr>
        <div>
            <h2>6 - Vowel Counter</h2>
            <form method="post" action="{{ route('vowels.count') }}">
                @csrf
                <textarea name="sentence" placeholder="Enter a sentence..." required></textarea>

                @isset($vowelCount)
                    <p class="center">The sentence "{{ $sentence }}" contains {{ $vowelCount }} vowel(s).</p>
                @endisset

                <input type="submit" value="Count Vowels">
            </form>
        </div>
        <hr>
        <div>
            <h2>7 - Grade Average</h2>
            <form method="post" action="{{ route('grades.calculate') }}">
                @csrf
                <p>Enter grades for three subjects:</p>
                <input type="number" placeholder="Grade 1" name="grade1" required><br>
                <input type="number" placeholder="Grade 2" name="grade2" required><br>
                <input type="number" placeholder="Grade 3" name="grade3" required><br>

                @isset($average)
                    <p class="center" style="color: black">Grade 1 = {{ $grade1 }}, Grade 2 = {{ $grade2 }}, Grade 3
                        = {{ $grade3 }}</p>
                    <p class="center">Average: {{ $average }}</p>
                @endisset

                <input type="submit" value="Calculate Average">
            </form>


        </div>
        <hr>
        <div>
            <h2>8 - Interest Calculation</h2>
            <form method="post" action="{{ route('investment.calculate') }}">
                @csrf
                <input type="number" placeholder="Enter initial capital" name="initial_capital" step="0.01" required><br>
                <input type="number" placeholder="Enter interest rate (%)" name="interest_rate" step="0.01" required><br>
                <input type="number" placeholder="Enter investment time (in months)" name="investment_time_months" required><br>

                @isset($finalValue)
                    <p class="center">Initial Capital: {{ number_format($initialCapital) }}</p>
                    <p class="center">Interest Rate: {{ $interestRate }}%</p>
                    <p class="center">Investment Time: {{ $investmentTimeMonths }} (in months)</p>
                    <p class="center">Final Value: {{ $finalValue }}</p>
                @endisset

                <input type="submit" value="Calculate Final Value">
            </form>
        </div>
    </body>
</html>
