<?php

namespace App\Repositories\Interfaces;

use App\Models\Client;

Interface ClientRepositoryInterface
{
    public function all();
    public function create(array $data);
    public function find($id);
    public function update($id, array $data);
    public function updateClient(array $data, Client $client);
    public function delete($id);
}
