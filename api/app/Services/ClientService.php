<?php

namespace App\Services;

use App\Models\Client;
use App\Repositories\ClientRepository;
use App\Repositories\Interfaces\ClientRepositoryInterface;

class ClientService extends AbstractService
{
    private ClientRepository $clientRepository;
    private $contactService;

    public function __construct(ClientRepositoryInterface $clientRepositoryInterface, ContactService $contactService, ClientRepository $clientRepository)
    {
        parent::__construct($clientRepositoryInterface);

        $this->contactService = $contactService;
        $this->clientRepository = $clientRepository;
    }

    public function getAllWithContacts()
    {
        return Client::with('contacts', 'contacts.types')->get();
    }

    public function createClientWithContacts(array $clientData, array $contactsData)
    {
        $data = $this->clientRepository->create($clientData);

        foreach ($contactsData as $contactData) {
            $this->contactService->createContactWithClient($data, $contactData);
        }
        return $data;
    }

    public function updateClientWithContacts(array $clientData, array $contactsData, Client $client)
    {
        // Update the client data
        $this->clientRepository->updateClient($clientData, $client);

        // Update the contacts data
        foreach ($contactsData as $contactData) {
            // If the contact already exists, update it; otherwise, create a new one
            if (isset($contactData['id'])) {
                $contact = $client->contacts()->find($contactData['id']);
                if ($contact) {
                    $contact->update($contactData);
                }
            } else {
                $client->contacts()->create($contactData);
            }
        }
    }
}
