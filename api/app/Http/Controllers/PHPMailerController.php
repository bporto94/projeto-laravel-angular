<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class PHPMailerController extends Controller
{
    public function email() {
        return view("email");
    }

    public function sendEmail(Request $request) {
        require base_path("vendor/autoload.php");
        $mail = new PHPMailer(true);     // Passing `true` enables exceptions

        try {

            // Email server settings
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';                     //  smtp host
            $mail->SMTPAuth = true;
            $mail->Username = 'restfullappcliente@gmail.com';   //  sender username
            $mail->Password = 'teste123';                       // sender password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;    // encryption - ssl/tls
            $mail->Port = 465;                                  // port - 587/465

            $mail->setFrom('restfullappcliente@gmail.com', 'Mailer');
            $mail->addAddress('restfullappcliente@gmail.com', 'Dev Bruno');

            $mail->addReplyTo('restfullappcliente@gmail.com');

            /*            if(isset($_FILES['emailAttachments'])) {
                            for ($i=0; $i < count($_FILES['emailAttachments']['tmp_name']); $i++) {
                                $mail->addAttachment($_FILES['emailAttachments']['tmp_name'][$i], $_FILES['emailAttachments']['name'][$i]);
                            }
                        }*/


            $mail->isHTML();                // Set email content format to HTML
            $mail->Subject = "Contato através portal Vale Cultura";
            $body = "Name: $request->name</ br>
                     E-mail: $request->email</ br>
            ";
            $mail->Body = $body . ' ' . $request->emailBody;

            if (!$mail->send()) {
                return back()->with("failed", "E-mail não enviado.")->withErrors($mail->ErrorInfo);
            } else {
                return back()->with("success", "E-mail enviado com sucesso.");
            }

        } catch (Exception) {
            return back()->with('error','Menssagem não pode ser enviada.');
        }
    }
}
