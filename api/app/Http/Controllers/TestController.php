<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        return view('test');
    }

    public function index2()
    {
        $primeNumbers = $this->getFirst10Primes();
        return view('test', compact('primeNumbers'));
    }

/*------------------------------------------------------- 1 -------------------------------------------------------*/
    public function calculate(Request $request)
    {
        $num1 = $request->input('num1');
        $num2 = $request->input('num2');
        $operator = $request->input('operator');
        $result = $this->performCalculation($num1, $num2, $operator);

        return view('test', compact('result'));
    }

    private function performCalculation($num1, $num2, $operator)
    {
        switch ($operator) {
            case '+':
                return $num1 + $num2;
            case '-':
                return $num1 - $num2;
            case '*':
                return $num1 * $num2;
            case '/':
                return $num2 !== 0 ? $num1 / $num2 : 'Cannot divide by zero';
            default:
                return 'Invalid operator';
        }
    }

/*------------------------------------------------------- 2 -------------------------------------------------------*/
    private function getFirst10Primes()
    {
        $primeNumbers = [];
        $count = 0;
        $num = 2;

        while ($count < 10) {
            $isPrime = true;

            if ($num <= 1) {
                $isPrime = false;
            } else {
                for ($i = 2; $i <= sqrt($num); $i++) {
                    if ($num % $i === 0) {
                        $isPrime = false;
                        break;
                    }
                }
            }

            if ($isPrime) {
                $primeNumbers[] = $num;
                $count++;
            }

            $num++;
        }

        return $primeNumbers;
    }

/*------------------------------------------------------- 3 -------------------------------------------------------*/

    public function factorial(Request $request)
    {
        $number = $request->input('number');
        $factorial = $this->calculateFactorial($number);
        return view('test', compact('factorial', 'number'));
    }

    private function calculateFactorial($num)
    {
        if ($num < 0) {
            return "Factorial is not defined for negative numbers.";
        }

        if ($num === 0 || $num === 1) {
            return 1;
        }

        $result = 1;
        for ($i = 2; $i <= $num; $i++) $result *= $i;

        return $result;
    }

/*------------------------------------------------------- 3 -------------------------------------------------------*/
    public function checkPalindrome(Request $request)
    {
        $word = $request->input('word');
        $isPalindrome = $this->isPalindrome($word);

        return view('test', compact('word', 'isPalindrome'));
    }

    private function isPalindrome($word)
    {
        $word = strtolower($word); // Convert to lowercase for case-insensitive comparison
        $reversedWord = strrev($word);
        return $word === $reversedWord;
    }

/*------------------------------------------------------- 5 -------------------------------------------------------*/
    public function generateTable(Request $request)
    {
        $number = $request->input('number');
        $tableData = $this->generateMultiplicationTable($number);
        return view('test', compact('tableData', 'number'));
    }

    private function generateMultiplicationTable($number)
    {
        $tableData = [];

        for ($i = 1; $i <= 10; $i++) {
            $tableData[] = [
                'multiplier' => $i,
                'result' => $number * $i,
            ];
        }

        return $tableData;
    }

/*------------------------------------------------------- 6 -------------------------------------------------------*/

    public function countVowels(Request $request)
    {
        $sentence = $request->input('sentence');
        $vowelCount = $this->countVowelsInString($sentence);
        return view('test', compact('vowelCount', 'sentence'));
    }

    private function countVowelsInString($string)
    {
        $vowels = array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');
        $count = 0;

        for ($i = 0; $i < strlen($string); $i++) {
            if (in_array($string[$i], $vowels)) {
                $count++;
            }
        }

        return $count;
    }

/*------------------------------------------------------- 7 -------------------------------------------------------*/

    public function calculateAverage(Request $request)
    {
        $grade1 = $request->input('grade1');
        $grade2 = $request->input('grade2');
        $grade3 = $request->input('grade3');
        $average = ($grade1 + $grade2 + $grade3) / 3;

        return view('test', compact('average', 'grade1', 'grade2', 'grade3'));
    }

/*------------------------------------------------------- 8 -------------------------------------------------------*/

    public function calculateFinalValue(Request $request)
    {
        $initialCapital = $request->input('initial_capital');
        $interestRate = $request->input('interest_rate');
        $investmentTimeMonths = $request->input('investment_time_months');

        $finalValue = $this->calculateFinalValueInvestment(($initialCapital), $interestRate, $investmentTimeMonths);

        return view('test', compact('initialCapital', 'interestRate', 'investmentTimeMonths', 'finalValue'));
    }

    private function calculateFinalValueInvestment($initialCapital, $interestRate, $investmentTimeMonths)
    {
        $monthlyInterestRate = $interestRate / 100 / 12;
        $finalValue = $initialCapital * pow(1 + $monthlyInterestRate, $investmentTimeMonths);
        return number_format($finalValue, '2');
    }
}
