<?php

use App\Http\Controllers\TestController;
use App\Http\Controllers\PHPMailerController;
use Illuminate\Support\Facades\Route;

/*
/ ----------------------------------------------------------------------------
/ Web Routes
/ ----------------------------------------------------------------------------
/
/ Here is where you can register web routes for your application. These
/ routes are loaded by the RouteServicePorvider within a group which
/ contains the "web" middleware group. Now create something great!
/
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', [TestController::class, "index"])->name('test');
Route::post('/calculator', [TestController::class, "calculate"])->name('calculator.calculate');
Route::post('/table', [TestController::class, "generateTable"])->name('table.generate');
Route::post('/factorial', [TestController::class, "factorial"])->name('factorial.calculate');
Route::post('/vowels', [TestController::class, "countVowels"])->name('vowels.count');
Route::post('/grades', [TestController::class, "calculateAverage"])->name('grades.calculate');
Route::post('/prime', [TestController::class, "index2"])->name('prime.prime-numbers');
Route::post('/palindrome', [TestController::class, "checkPalindrome"])->name('palindrome.check');
Route::post('/investment', [TestController::class, "calculateFinalValue"])->name('investment.calculate');


Route::get("email", [PHPMailerController::class, "email"])->name("email");

Route::post("send-email", [PHPMailerController::class, "sendEmail"])->name("send-email");
