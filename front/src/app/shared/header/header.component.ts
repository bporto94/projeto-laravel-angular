import {Component, OnInit} from '@angular/core';
import {MegaMenuItem} from "primeng/api";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  items: MegaMenuItem[] | undefined;

  ngOnInit(): void {
    this.items = [
      {
        label: 'Home',
        routerLink: '/',
      },
      {
        label: 'Clients',
        icon: 'pi pi-fw pi-users',
        routerLink: 'clients'
      },
      {
        label: 'Contact Types',
        icon: 'pi pi-fw pi-book',
        routerLink: 'types'
      },
    ];
  }

}
