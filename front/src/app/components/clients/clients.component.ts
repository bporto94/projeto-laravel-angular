import {Component, Input, OnInit} from '@angular/core';
import {ClientsService} from "../../services/clients.service";
import {FormArray, FormBuilder, Validators} from "@angular/forms";
import {ClientsModel} from "../../models/clients.model";
import {MensagemService} from "../../services/messages.service";
import {ConfirmationService} from "primeng/api";
import {TypesModel} from "../../models/types.model";
import {TypeService} from "../../services/type.service";

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  clients: any[] = [];
  @Input() client: ClientsModel | null | undefined;
  data: ClientsModel = new ClientsModel();
  form: any;
  formContacts: any = FormArray;
  editData: any;
  types: TypesModel[] | undefined;
  visible: boolean = false;
  exibeFormEditClient: boolean = false;

  constructor(
    protected clientsService: ClientsService,
    protected typesService: TypeService,
    protected formBuilder: FormBuilder,
    protected mensagemService: MensagemService,
    protected confirmationService: ConfirmationService
  ) {
  }

  ngOnInit() {
    this.listClients();
    this.initForm();
    this.listTypes();
  }

  initForm() {
    this.form = this.formBuilder.group({
      id: [null],
      name: [null, [Validators.required]],
      age: [null, [Validators.required]],
      contacts: this.formBuilder.array([])
    })
  }

  listClients() {
    this.clientsService.getClients().subscribe((data: ClientsModel[]) => {
      this.clients = data;
    });
  }

  listTypes() {
    this.typesService.getType().subscribe((data: TypesModel[]) => {
      this.types = data;
    });
  }

  get contacts() {
    return this.form.get("contacts") as FormArray;
  }

  createClient() {
    this.clientsService.addClient(this.form.value).subscribe(value => {
      this.form.value = new ClientsModel();
      console.log(this.form.value);
      this.listClients();
    });
  }

  showCliente(id: any) {
    this.visible = true;
    return this.clientsService.getClientById(id).subscribe((data: ClientsModel) => {
      this.client = data;
    });
  }

  updateCliente() {
    this.exibeFormEditClient = false;
    this.clientsService.updateClient(this.form.value.id, this.form.value).subscribe(data => {
      this.client = null;
      this.form.reset();
      this.listClients();
    });
  }

  deleteCliente(id: any) {
    this.confirmDelete(id);
    this.listClients();
  }

  confirmDelete(id: any) {
    this.confirmationService.confirm({
      message: 'Deseja realmente excluir o cliente?',
      accept: () => {
        this.clientsService.deleteClient(id)
          .subscribe({
            next: res => {
              if (res.sucesso) {
                this.mensagemService.addMessageSuccess('Exclusão realizada com sucesso.');
                console.log('sucesso');
              } else {
                this.mensagemService.addMessageError(res.mensagem);
                console.log('error');
              }
            }, error: err => {
              if (err != null && err.error != null && err.error.mensagem != null) {
                this.mensagemService.addMessageError(err.error.mensagem);
              }
            }
          });
        this.listClients();
      },
      acceptLabel: 'Sim',
      rejectLabel: 'Não',
      acceptButtonStyleClass: 'p-button-success',
      rejectButtonStyleClass: 'p-button-danger'
    });
  }

  editCliente(id: any) {
    this.clientsService.getClientById(id).subscribe(data => {
      this.client = data;
      this.editData = data;
      this.exibeFormEditClient = true;
      if (this.editData.contacts != null) {
        for (let i = 0; i < this.editData.contacts.length; i++) {
          this.addContacts();
        }
      }
      this.form.patchValue({
        id: this.editData.id,
        name: this.editData.name,
        age: this.editData.age,
        contacts: this.editData.contacts
      });
    });
  }

  addContacts() {
    this.formContacts = this.form.get("contacts") as FormArray;
    this.formContacts.push(this.contactRow());
  }

  contactRow() {
    return this.formBuilder.group({
      id: this.formBuilder.control(''),
      description: this.formBuilder.control(''),
      type_id: this.formBuilder.control('')
    })
  }

  removeContact(index: any) {
    this.formContacts = this.form.get("contacts") as FormArray;
    this.formContacts.removeAt(index);
  }

  closeDialogCliente() {
    this.exibeFormEditClient = false;
    this.form.reset();
  }

  closeDialog() {
    this.client = null;
  }
}
