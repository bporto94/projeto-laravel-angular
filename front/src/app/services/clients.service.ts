import {HttpClient} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";
import {Injectable} from "@angular/core";
import {ClientsModel} from "../models/clients.model";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  constructor(private http: HttpClient) {
  }

  getClients(): Observable<ClientsModel[]> {
    return this.http.get<ClientsModel[]>(`${environment.apiUrl}/client`).pipe(
      catchError(this.handleError)
    );
  }

  getClientById(id: number): Observable<ClientsModel> {
    return this.http.get<ClientsModel>(`${environment.apiUrl}/client/`.concat(String(id))).pipe(
      catchError(this.handleError)
    );
  }

  addClient(client: ClientsModel): Observable<ClientsModel> {
    return this.http.post<ClientsModel>(`${environment.apiUrl}/client`, client).pipe(
      catchError(this.handleError)
    );
  }

  updateClient(id: number, client: ClientsModel): Observable<ClientsModel> {
    return this.http.put(`${environment.apiUrl}/client/`.concat(String(id)), client).pipe(
      catchError(this.handleError)
    );
  }

  deleteClient(id: any): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/client/`.concat(id)).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: any): Observable<any> {
    console.error('Ocorreu um erro:', error);
    return throwError('Algo deu errado; por favor, tente novamente mais tarde.');
  }

}
